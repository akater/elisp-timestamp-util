;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'timestamp-util
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  org-files-in-order '("timestamp-util")
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda ()
              (require 'org-src-elisp-extras)
              (require 'mmxx-macros-applications)
              (require 'looking-through)
              (require 'akater-misc-buffers-macs)
              (setq debug-on-error t)))
